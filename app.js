'use strict';

const rsServer = require('./api/server');
//const reportServer = require('./reports/server');
const fs = require('fs');

var configFile = 'config.json';

function replaceEnvValues(data) {
	// Stolen from https://stackoverflow.com/questions/21363912/how-to-resolve-a-path-that-includes-an-environment-variable-in-nodejs
	let replaced = data.replace(/%([^%]+)%/g, (_,n) => process.env[n].replace("\\", "\\\\"));
	return replaced;
}

function loadStartlistConfigFromFile(startlist) {
    var configFile = startlist.configFile;
    if (!fs.existsSync(configFile)) {
        let err = `File ${configFile} does not exist.`;
        console.error(err);
        throw new Error(err);
    }
    var configFileLastModified = getFileModifiedDate(configFile);
    var id = startlist.id;
    var rawData = fs.readFileSync(configFile, 'utf8'); //, function (err, data) {
    var fixedData = replaceEnvValues(rawData);
    var configNode;
    try {
        configNode = JSON.parse(fixedData);
    } catch (err) {
        console.error("Error parsing startlist config at " + configFile);
        throw err;
    }
    // Retain only 'id' and configFile
    for (var slmember in startlist) delete startlist[slmember];
    startlist.id = id;
    startlist.configFile = configFile;
    startlist.configFileLastModifiedDate = configFileLastModified;
    startlist.get = function () { return getUpdatedConfig(startlist); };
    for (var cnmember in configNode) startlist[cnmember] = configNode[cnmember];
}

function getFileModifiedDate(file) {
    var stats = fs.statSync(file);
    var mtime = stats.mtime;
    return mtime;
}

function isConfigFileModified(startlist) {
    var stats = fs.statSync(startlist.configFile);
    var mtime = stats.mtime;
    if (mtime > startlist.configFileLastModifiedDate) {
        return true;
    }
    return false;
}

function getUpdatedConfig(startlist) {
    if (isConfigFileModified(startlist)) {
        console.log("Config file " + startlist.configFile + " has been modified since last use - re-loading from disk.");
        loadStartlistConfigFromFile(startlist);
    }
    return startlist;
}

function loadStartlistConfigs(config) {
    var lists = config.startLists;
    let failed = false;
    for (var i = 0;i < lists.length;i++) {
        var startlist = lists[i];
        if (startlist.configFile) {
            try {
                loadStartlistConfigFromFile(startlist);
            } catch (err) {
                console.error(`Couldn't read startlist config referenced in ${configFile}: ${err.message}`);
                failed = true;
            }
        }
    }
    if (failed) {
        throw Error("Failed reading one or more config files.");
    }
}

async function readConfig(fileToRead, onConfigRead) {
	if (!fileToRead) {
		fileToRead = configFile;
	}
    if (!fs.existsSync(fileToRead)) {
        console.error(`Input config file ${fileToRead} does not exist.`);
    }
	fs.readFile(fileToRead, 'utf8', function (err, data) {
        try {
		var fixedData = replaceEnvValues(data);

        var config;
        try {
            config = JSON.parse(fixedData);
        } catch (err) {
            console.error("Failed parsing config file at " + fileToRead);
            throw err;
        }
        loadStartlistConfigs(config);
		console.log("Config loaded from " + fileToRead);
		onConfigRead(config);
        return config;
        } catch (err) {
            console.error(`Failed while trying to read file ${fileToRead}`);
            throw err;
        }
	});

}

function initServers(config) {
	rsServer.init(config);
//	try {
//		reportServer.init(config);
//	} catch (err) {
//		console.log(err);
//	}
}

try {
	readConfig(configFile, initServers);
} catch (e) {
	console.log("Exception while reading config " + configFile);
	console.log(e);
}

