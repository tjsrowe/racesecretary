'use strict';

const getDobYear = require('./getDobYear');
const getRiderPrintId = require('./getRiderPrintId');

function getRiderAgeInYear(eventYear, rider) {
    if (rider.DOB) {
        var riderYear = getDobYear(rider.DOB);
        var riderAge = eventYear - riderYear;
        return riderAge;
    } else {
        throw Error("No DOB provided for rider " + getRiderPrintId(rider));
    }
}

module.exports = getRiderAgeInYear;