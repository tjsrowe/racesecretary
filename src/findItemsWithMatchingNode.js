'use strict';

function findItemsWithMatchingNode(arr, propertyName, value) {
    var results = Array();
    for (var i = 0;i < arr.length;i++) {
        var item = arr[i];
        if (item && item[propertyName] == value) {
            results.push(item);
        }
    }
    return results;
}

module.exports = findItemsWithMatchingNode;