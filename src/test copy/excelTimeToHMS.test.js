
'use strict';

const excelTimeToHMS = require('../excelTimeToHMS');

var testData = [{ "source": "0.008091667", "target": "11:39.1"}]

test('Finds a present value', () => {
    expect(excelTimeToHMS(testData[0].source)).toBe(testData[0].target);
});