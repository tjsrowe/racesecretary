'use strict';

function findWithMatchingNode(arr, propertyName, value) {
    for (var i = 0;i < arr.length;i++) {
        var item = arr[i];
        if (item && item[propertyName] == value) {
            return item;
        }
    }
    return null;
}

module.exports = findWithMatchingNode;