'use strict';

const findWithMatchingNode = require('../src/findWithMatchingNode');
const getRiderPrintId = require('../src/getRiderPrintId');

function getCategoryForRider(eventData, rider, additionalDebugFields = undefined) {
    var catCode = rider.EventCategoryCode;
    if (rider.EventCategoryCode == undefined) {
        var debugInfoSuffix = "";
        if (additionalDebugFields) {
            if (Array.isArray(additionalDebugFields)) {
                debugInfoSuffix = " (";
                additionalDebugFields.forEach((f) => {
                    debugInfoSuffix += `${f}: ${rider[f]}`;
                });
                debugInfoSuffix += ")";
            } else {
                debugInfoSuffix = ` (${additionalDebugFields}: ${rider[additionalDebugFields]})`;
            }
        }
        console.debug("No EventCategoryCode defined for rider " + getRiderPrintId(rider) + debugInfoSuffix);
        return undefined;
    }
    var category = findWithMatchingNode(eventData.categoryList, 'CategoryCode', catCode);
    return category;
}

module.exports = getCategoryForRider;