'use strict';

function excelTimeToHMS(serial) {
    var hours = serial * 24.0;
    var minutes = hours * 60;
    var seconds = minutes * 60;

    var remainingSeconds = seconds % 60;
    var remainingMinutes = minutes % 60;
    return Math.floor(remainingMinutes) + ":" + remainingSeconds.toFixed(1);
 }

 module.exports = excelTimeToHMS;