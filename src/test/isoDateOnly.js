'use strict';

function isoDateOnly(d) {
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d);
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    var output = `${ye}-${mo}-${da}`;
    return output;
}

module.exports = isoDateOnly;