'use strict';

const findById = require('../findById');

var testData = [
    { "id": "1", "value": "one" },
    { "id": "2", "value": "two" },
    { "id": "3", "value": "three" },
    { "id": "4", "value": "four" }
];

test('Finds a present value', () => {
    expect(findById(testData, 1).value).toBe("one");
});

test('Finds a second present value', () => {
    expect(findById(testData, "2").value).toBe("two");
});

test('To not find a non-existent value', () => {
    expect(findById(testData, "5")).toBe(null);
});
