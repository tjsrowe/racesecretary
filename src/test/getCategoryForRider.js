'use strict';

const findWithMatchingNode = require('../src/findWithMatchingNode');
const getRiderPrintId = require('../src/getRiderPrintId');

function getCategoryForRider(eventData, rider) {
    var catCode = rider.EventCategoryCode;
    if (rider.EventCategoryCode == undefined) {
        console.debug("No EventCategoryCode defined for rider " + getRiderPrintId(rider));
        return undefined;
    }
    var category = findWithMatchingNode(eventData.categoryList, 'CategoryCode', catCode);
    return category;
}

module.exports = getCategoryForRider;