'use strict';

function getRiderPrintId(rider) {
    var printableName = rider.FirstName.trim() + " " + rider.Surname.toUpperCase().trim();
    return printableName;
}

module.exports = getRiderPrintId;