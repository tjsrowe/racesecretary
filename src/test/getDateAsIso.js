'use strict';

const excelDateToIsoDate= require('./excelDateToIsoDate');
const is_number = require('is-number');

function getDateAsISO(str) {
    if (is_number(str)) {
        return excelDateToIsoDate(str);
    } else if (str) {
    try {
        if (str.indexOf("/") >= 2) {
            // means we have d/m/y or m/d/y
            var parts = str.split("/");
            var retVal;
            var day;
            var month;
            var year;

            if (parts.length == 3) {
                day = parts[0];
                month = parts[1];
                if (parts[2] < 31) {
                    year = 2000 + parseInt(parts[2]);
                } else if (parts[2] < 100) {
                    year = 1900 + parseInt(parts[2]);
                } else {
                    year = parseInt(parts[2]);
                }
            } else if (parts.length == 2) {
                // We kinda have to assume it's the current year with only the month and day being specified.
                year = (new Date()).getFullYear();
                if (parts[0] > 12) {
                    // Stupid American date formats.
                    month = parts[0];
                    day = parts[1];
                } else {
                    month = parts[1];
                    day = parts[0];
                }
            }
            retVal = year + "-" + month + "-" + day;
            return retVal;
        }
    } catch (err) {
        console.trace("Can't parse date " + str);
        throw err;
    }
    }
    return str;
}

module.exports = getDateAsISO;