'use strict';

const isoDate = require('../getDateAsIso');

var testData = [
    { "source": "05/05/10", "target": "2010-05-05"},
    { "source": "29/12/25", "target": "2025-12-29"},
    { "source": "12/04/87", "target": "1987-04-12"},
    { "source": "05/05/2011", "target": "2011-05-05"}
];

test('Test parsing two-character year string', () => {
    expect(isoDate(testData[0].source)).toBe(testData[0].target);
    expect(isoDate(testData[1].source)).toBe(testData[1].target);
    expect(isoDate(testData[2].source)).toBe(testData[2].target);
});

test('Test parsing four-character year string', () => {
    expect(isoDate(testData[3].source)).toBe(testData[3].target);
});