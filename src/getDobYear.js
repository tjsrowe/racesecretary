'use strict';

const getDateAsISO = require('./getDateAsIso');

function getDobYear(dob) {
    var dobDate = getDateAsISO(dob);
    var yearPart = dobDate.substring(0, 3);
    return yearPart;
}

module.exports = getDobYear;