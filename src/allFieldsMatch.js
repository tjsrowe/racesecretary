'use strict';

const is_number = require('is-number');

function noValue(val) {
    if (val == null) {
        return true;
    }
    if (val == undefined) {
        return true;
    }
    if (is_number(val)) {
        return false;
    }
    if (val.trim() == '') {
        return true;
    }
    return false;
}

function fieldMatches(leftObject, leftField, rightObject, rightField, caseSensitive) {
    if (leftObject == null && rightObject != null) {
        return false;
    }
    if (leftObject != null && rightObject == null) {
        return false;
    }
    var leftValue = leftObject[leftField];
    var rightValue = rightObject[rightField];

    if (noValue(leftValue) || noValue(rightValue)) {
        return false;
    }
    if (leftValue == rightValue) {
        return true;
    }
    if (leftValue != null && rightValue == null) {
        return false;
    }
    if (!caseSensitive && leftValue != null && rightValue != null && leftValue.toString().toLowerCase() == rightValue.toString().toLowerCase()) {
        return true;
    }

    return false;
}

function allFieldsMatch(leftObject, rightObject, fields, caseSensitive) {
    var matches = true;
    var keys = Object.keys(fields);
    for (var keyNo = 0;keyNo < keys.length;keyNo++) {
        var leftKey = keys[keyNo];
        var rightKey = fields[leftKey];

        if (!fieldMatches(leftObject, leftKey, rightObject, rightKey, caseSensitive)) {
            matches = false;
            break;
        }
    }
    return matches;
}

module.exports = allFieldsMatch;