'use strict';

function getEventYear(eventData) {
    var eventDate = new Date(eventData.RaceDate);
    if (!eventDate) {
        eventDate = new Date();
    }
    var eventYear = eventDate.getFullYear();
    return eventYear;
}

module.exports = getEventYear;