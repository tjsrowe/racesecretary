'use strict';

function findById(arr, id) {
    for (var i = 0;i < arr.length;i++) {
        if (arr[i] && arr[i].id == id) {
            return arr[i];
        }
    }
    return null;
}

module.exports = findById;