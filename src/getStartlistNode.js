'use strict';

const findById = require('./findById');

function getStartListNode(startLists, id) {
    var startListNode = findById(startLists, id);
    startListNode = startListNode.get();
    return startListNode;
}

module.exports = getStartListNode;