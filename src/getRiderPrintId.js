'use strict';

function getRiderPrintId(rider) {
    var printableName = rider.FirstName.toString().trim() + " " + rider.Surname.toString().toUpperCase().trim();
    return printableName;
}

module.exports = getRiderPrintId;