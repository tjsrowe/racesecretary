'use strict';

function matchColumnName(searchColumns, header) {
    if (header == null || header == undefined) {
        console.log("Got passed undefined header.");
        return null;
    }
    var cleanedHeaderName = header.toLowerCase().trim();
    for (var i = 0;i < searchColumns.length;i++) {
        if (searchColumns[i] == undefined || searchColumns[i] == null) {
            continue;
        }
        var detailLower = searchColumns[i].toLowerCase();
        if (cleanedHeaderName == detailLower) {
            return searchColumns[i];
        }
    }
    return null;
}

var DataFile = function(coreDetails, headers) {
    var df = {
        "columns": {},
        "searchColumns": coreDetails
    };
    for (var col = 0;col < headers.length;col++) {
        var columnName = headers[col];
        var matched = matchColumnName(coreDetails, columnName);
        if (matched) {
            df.columns[matched] = col;
            continue;
        }
    }

    df.objectFromRow = function(row) {
        var output = {};
        for (var data = 0;data < this.searchColumns.length;data++) {
            var dataElement = this.searchColumns[data];
            var colNo = this.columns[dataElement];
            output[dataElement] = row[colNo];
        }
        return output;
    } 
    return df;
}

module.exports = DataFile;