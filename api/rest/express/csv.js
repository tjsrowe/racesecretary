'use strict';
const cors = require('cors');

exports.init = function(rest, corsOptions, csvController) {
    rest.get('/csv/:id', cors(corsOptions), function (request, response) {
        var id = request.params.id;
        csvController.startListAsJson(id,
            function(result) {
                response.send(JSON.stringify(result));
        }, function (error) {

        });
    });
};