'use strict';
const cors = require('cors');

exports.init = function(rest, corsOptions, regolistController) {
    rest.get('/racerego/json/:id', cors(corsOptions), function (request, response) {
        var id = request.params.id;
        regolistController.registrationListAsJson(id,
            function(result) {
                response.send(JSON.stringify(result));
        }, function (error) {
            console.debug(error);
        });
    });

    rest.get('/racerego/csv/:id', cors(corsOptions), function (request, response) {
        var id = request.params.id;
        regolistController.registrationListAsCsv(id, regolistController.getRegoListColumns(),
            function(result) {
                var filename = 'regolist_' + id + '.csv';
                response.header('Content-Type', 'text/csv');
                response.header('Content-Disposition', 'inline; filename="' + filename + '"');
                response.send(result);
        }, function (error) {
            console.debug(error);
        });
    });
};