'use strict';
const { Console } = require('console');
const cors = require('cors');
const fs = require('fs');

function streamFileToResponse(response, filePath, contentType, clientFileName) {
    var stat = fs.statSync(filePath);

    if (!clientFileName) {
        clientFileName = stat.filename;
    }

    response.writeHead(200, {
        'Content-Type': contentType,
        'Content-Length': stat.size,
        'Content-Disposition': 'attachment; filename="' + clientFileName + '"',
        'Transfer-Encoding': 'chunked'
    });

    //var readStream = fs.createReadStream(filePath);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    console.log("Writing file to output stream.");
    fs.readFileSync(filePath, function(err, data) {
        response.end(data);
    });
/*    readStream.pipe(response)
    .on('finish', function() {
        console.log("Ending response.");
        readStream.close();
    }).on('close', function() {
        response.send();
        response.end();
    });
    */
}

exports.init = function(rest, corsOptions, startlistController) {
    rest.get('/startlist/json/:id', cors(corsOptions), function (request, response) {
        var id = request.params.id;
        startlistController.getStartlistRiders(id,
            function(result) {
                response.send(JSON.stringify(result));
        }, function (error) {
            console.debug(error);
        });
    });

    rest.get('/startlist/csv/:id', cors(corsOptions), function (request, response) {
        var id = request.params.id;
        startlistController.startListAsCsv(id, startlistController.getStartlistColumns(id),
            function(result) {
                var filename = 'startlist_' + id + '.csv';
                response.header('Content-Type', 'text/csv');
                response.header('Content-Disposition', 'inline; filename="' + filename + '"');
                response.send(result);
        }, function (error) {
            console.debug(error);
        }, null);
    });

    rest.get('/startlist/race/:id/:racenum', cors(corsOptions), function (request, response) {
        var id = request.params.id;
        var racenum = request.params.racenum;
        if (racenum == 'all') {
            racenum = null;
        } else if (racenum.indexOf(',') > 0) {
            racenum = racenum.split(",");
        }
        startlistController.startListAsCsv(id, startlistController.getStartlistColumns(id),
            function(result) {
                var filename = 'startlist_' + id + '_race_' + racenum + '.csv';
                response.header('Content-Type', 'text/csv');
                response.header('Content-Disposition', 'inline; filename="' + filename + '"');
                response.send(result);
        }, function (error) {
            console.debug(error);
        }, racenum);
    });

    rest.get('/startlist/xlsx/:id', cors(corsOptions), function (request, response) {
        var id = request.params.id;
        startlistController.startListAsXlsReport(id,
            function(tmpFilePath) {
                var clientFileName = 'startlist_' + id + '.xlsx';
                var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                streamFileToResponse(response, tmpFilePath, contentType, clientFileName);
        }, function (error) {
            console.debug(error);
        });
    });
};