'use strict';
const cors = require('cors');

exports.init = function(rest, corsOptions, eventlistController) {
    rest.get('/list', cors(corsOptions), function (request, response) {
        eventlistController.listAsHtml(
            function(htmlBody) {
                response.send(htmlBody);
        }, function (error) {
            console.debug(error);
        });
    });
};
