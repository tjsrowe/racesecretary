'use strict';

const express = require('express')
var rest = express();
var morgan = require('morgan');
rest.use(morgan('combined'));

var session = require('express-session');
const uuidv1 = require('uuid/v1');
const cors = require('cors');

const dataFileController = require('../controller/dataFileController');
const startlistController = require('../controller/startlistController');
const csvRest = require('./rest/express/csv');
const startlistRest = require('./rest/express/startlist');
const registrationListRest = require('./rest/express/regolist');
const registrationListController = require('../controller/registrationListController');
const startlistReportController = require('../controller/startlistReportController');

const eventlistRest = require('./rest/express/eventlist');
const eventlistController = require('../controller/eventlistController');

function configureCors(config) {
    var allowedOrigins = config.apiServer.allowedOrigins;
    const corsOptions = {
        origin: (origin, callback) => {
            if (allowedOrigins.includes(origin) || !origin) {
                callback(null, true);
            } else {
            callback(new Error('Origin ' + origin + ' not allowed by CORS'));
            }
        }
    };
    return corsOptions;
}

exports.init = function(lConfig, onComplete) {
    config = lConfig;
    let rest = createRestServer(config);
    //exports.createDao(config);
	var listenPort = config.apiServer.port;
	rest.listen(listenPort, () => console.log(`Listening for data model requests on port ${listenPort} - http://localhost:${listenPort}/list`));
	if (onComplete) {
		onComplete();
	}
};

function createRestServer(config) {
    if (rest.get('env') === 'production') {
        rest.set('trust proxy', 1) // trust first proxy
        sess.cookie.secure = true // serve secure cookies
      }

    let corsOptions = configureCors(config);

    rest.options('*', cors(corsOptions));
    rest.use(session(sess));

    console.debug("Creating data file controller.");
    dataFileController.setConfig(config);

    registrationListController.setConfig(config);
    registrationListController.setDataFileController(dataFileController);

    eventlistController.setConfig(config);

    console.debug("Creating Start List Controller");
    startlistController.setConfig(config);
    startlistController.setDataFileController(dataFileController);
    startlistController.setRegistrationListController(registrationListController);
    startlistController.setStartlistReportController(startlistReportController);

    console.debug("Creating CSV REST");
    csvRest.init(rest, corsOptions, dataFileController);

    console.debug("Creating Start List REST");
    startlistRest.init(rest, corsOptions, startlistController);

    console.debug("Creating Registration List REST");
    registrationListRest.init(rest, corsOptions, registrationListController);

    console.debug("Creating Event List REST");
    eventlistRest.init(rest, corsOptions, eventlistController);
    
    return rest;
}

var sess = {
    secret: 'cookie development server',
    cookie: {},
    genid: function(req) {
      return uuidv1() // use UUIDs for session IDs
    }
  }
  
var config = {};
  

