'use strict';

var config = {};

var EventlistController = function() {

};

EventlistController.setConfig = function(configObject) {
    config = configObject;
};

EventlistController.listAsHtml = function(callback) {
    var html = '<html><body>';
    var lists = config.startLists;
    for (var i = 0;i < lists.length;i++) {
        var startlist = lists[i];
        var list = startlist.get();

        var header = '<h2>' + list.id;
        header += '</h2>';

        html += header;

        var regoList = '<p>Rego list: ';
        regoList += '<a href="/racerego/json/' + list.id + '">JSON</a> ';
        regoList += '<a href="/racerego/csv/' + list.id + '">CSV</a> ';
        regoList += '</p>';

        var startList = '<p>Start list: ';
        startList += '<a href="/startlist/json/' + list.id + '">JSON</a> ';
        startList += '<a href="/startlist/csv/' + list.id + '">CSV</a> ';
        startList += '<a href="/startlist/xlsx/' + list.id + '">XLS</a> ';
        startList += '</p>';

        html += regoList;
        html += startList;

    }
    html += '</body></html>';
    callback(html);
}

module.exports = EventlistController;