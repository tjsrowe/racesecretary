'use strict';
const findWithMatchingNode = require('../src/findWithMatchingNode');
const csvstringify = require('csv-stringify');
const flatten = require('flat');
const getRiderPrintId = require('../src/getRiderPrintId');
const getCategoryForRider = require('../src/getCategoryForRider');
const getStartListNode = require('../src/getStartListNode');

var SLRC;
var SLC;

var StartlistController = function() {
    StartlistController.config = {};
};
SLC = StartlistController;

StartlistController.setDataFileController = function(controller) {
    StartlistController.dataFileController = controller;
};

StartlistController.setRegistrationListController = function(controller) {
    StartlistController.registrationListController = controller;
}

StartlistController.setConfig = function(configObject) {
    StartlistController.config = configObject;
};

StartlistController.setStartlistReportController = function(controller) {
    SLRC = controller;
    SLRC.setStartlistController(StartlistController);
};

function getRiderRankingForRankingType(rankingSet, rider) {
    if (!rider.ranking) {
        return null;
    }
    var ranking = rider.ranking[rankingSet.type];
    if (ranking) {
        return ranking.Rank;
    }
    return null;
}

function shouldIgnoreRank(rider) {
    if (rider && !rider.IgnoreRank) {
        return false;
    }
    return true;
}

function compareRiderRanking(eventData, x, y) {
    eventData.rankingData.sort(function(x, y) {
        return x.order - y.order;
    });
    if (shouldIgnoreRank(x) && !shouldIgnoreRank(y)) {
        return 1;
    } else if (!shouldIgnoreRank(x) && shouldIgnoreRank(y)) {
        return 1;
    } else if (shouldIgnoreRank(x) && shouldIgnoreRank(y)) {
        return 0;
    }
    for (var i = 0;i < eventData.rankingData.length;i++) {
        var rankingSet = eventData.rankingData[i];
        var rankx = getRiderRankingForRankingType(rankingSet, x);
        var ranky = getRiderRankingForRankingType(rankingSet, y);

        var maxRank = rankingSet.maximumRank;
        if (maxRank && rankx > maxRank) {
            rankx = null;
        }

        if (maxRank && ranky > maxRank) {
            ranky = null;
        }

        if (rankx && !ranky) {
            return -1;
        }
        if (!rankx && ranky) {
            return 1;
        }
        if (rankx && ranky) {
            return rankx - ranky;
        }
    }
    return 0;
}

function getRaceWaveForCategory(eventData, category) {
    var raceWave = category.RaceWave;
    return raceWave;
}

function getStartOrderForCategory(eventData, category) {
    var startOrder = category.StartOrder;
    return startOrder;
}

function getRaceWaveForRider(eventData, rider) {
    var categoryForRider = getCategoryForRider(eventData, rider);
    var raceWave = getRaceWaveForCategory(eventData, categoryForRider);
    return raceWave;
}

function getWaveOrderForRider(eventData, rider) {
    var categoryForRider = getCategoryForRider(eventData, rider);
    var raceWave = getStartOrderForCategory(eventData, categoryForRider);
    return raceWave;
}

StartlistController.getRaceForRider = function(eventData, rider) {
    var categoryForRider = getCategoryForRider(eventData, rider);
    if (categoryForRider == null) {
        if (rider.Category == undefined) {
            console.warn("Rider " + getRiderPrintId(rider) + " has no category data.");
            return undefined;
        } else {
            throw Error("Rider " + getRiderPrintId(rider) + " is entered in a category " + rider.Category + " which does not exist.");
        }
    } else {
        var raceForCategory = getRaceForCategory(eventData, categoryForRider);
        return raceForCategory;
    }
}

function getRaceForCategory(eventData, category) {
    if (category == undefined || category == null) {
        throw new Error("Tried to get race number for undefined category");
    }
    var raceNo = category.RaceNumber;
    var race = findWithMatchingNode(eventData.racesData, 'RaceNumber', raceNo);
    if (race == null) {
        throw Error("Category " + category.CategoryName + " was assigned to a race number " + raceNo + ", but race " + raceNo + " is not defined in races config file.");
    }
    return race;
}

function compareRaceNumber(eventData, x, y) {
    if (x == null && y != null) {
        return -1;
    }
    if (y == null && x != null) {
        return 1;
    }
    if (x == null && y == null) {
        return 0;
    }

    var racex = SLC.getRaceForRider(eventData, x);
    var racey = SLC.getRaceForRider(eventData, y);
    if (!racex) {
        racex = 0;
    }
    if (!racey) {
        racey = 0;
    }
    return racex.RaceNumber - racey.RaceNumber;
}

function compareRaceWave(eventData, x, y) {
    if (x == null && y != null) {
        return -1;
    }
    if (y == null && x != null) {
        return 1;
    }
    if (x == null && y == null) {
        return 0;
    }

    var raceWaveX = getRaceWaveForRider(eventData, x);
    var raceWaveY = getRaceWaveForRider(eventData, y);
    return raceWaveX - raceWaveY;
}

function compareCategoryInWave(eventData, x, y) {
    if (x == null && y != null) {
        return -1;
    }
    if (y == null && x != null) {
        return 1;
    }
    if (x == null && y == null) {
        return 0;
    }

    var waveOrderX = getWaveOrderForRider(eventData, x);
    var waveOrderY = getWaveOrderForRider(eventData, y);
    return waveOrderX - waveOrderY;
}

function compareStartGroupOrder(eventData, x, y) {
    if (x == null && y != null) {
        return -1;
    }
    if (y == null && x != null) {
        return 1;
    }
    if (x == null && y == null) {
        return 0;
    }
    var raceNumberComparison = compareRaceNumber(eventData, x, y);
    if (raceNumberComparison != 0) {
        return raceNumberComparison;
    }

    var raceWaveComparison = compareRaceWave(eventData, x, y);
    if (raceWaveComparison != 0) {
        return raceWaveComparison;
    }

    var categoryInWaveComparison = compareCategoryInWave(eventData, x, y);
    if (categoryInWaveComparison != 0) {
        return categoryInWaveComparison;
    }
    return 0;
}

function sortStartlistOrder(eventData) {
    if (eventData.racesData == null) {
        throw Error("Can't sort start list before loading races data.");
    }
    eventData.riderData.sort(function(x, y) {
        var startGroupComparison = compareStartGroupOrder(eventData, x, y);
        if (startGroupComparison != 0) {
            return startGroupComparison;
        }

        var riderRankingComparison = compareRiderRanking(eventData, x, y);
        if (riderRankingComparison != 0) {
            return riderRankingComparison;
        }
    });
}

StartlistController.isRiderInRace = function(eventData, rider, race) {
    return isRiderInRaceNumber(eventData, rider, race.RaceNumber);
};

function isRiderInRaceNumber(eventData, rider, raceNumber) {
    var race = SLC.getRaceForRider(eventData, rider);
    if (Array.isArray(raceNumber)) {
        for (var i = 0;i < raceNumber.length;i++) {
            var checkNum = raceNumber[i];
            if (race.RaceNumber == checkNum) {
                return true;
            }
        }
    } else if (race.RaceNumber == raceNumber) {
        return true;
    }
    return false;
}

function limitEntriesToRaceNumber(eventData, raceNumber) {
    var riders = eventData.riderData;
    var outputRiderList = Array();
    for (var i = 0; i < riders.length;i++) {
        var checkRider = riders[i];
        if (isRiderInRaceNumber(eventData, checkRider, raceNumber)) {
            outputRiderList.push(checkRider);
        }
    }
    eventData.riderData = outputRiderList;
}

StartlistController.getStartlistRiders = function(id, onSuccess, onFailure, limitToRaceNumber) { 
    StartlistController.getStartlistData(id, function(eventData) {
        onSuccess(eventData.riderData, eventData.startlistColumns);
    }, onFailure, limitToRaceNumber);
};

StartlistController.getStartlistData = function(id, onSuccess, onFailure, limitToRaceNumber) {
    StartlistController.registrationListController.getRegistrationListData(id, function (eventData) {
        if (limitToRaceNumber) {
            limitEntriesToRaceNumber(eventData, limitToRaceNumber);
        }
        sortStartlistOrder(eventData);
        onSuccess(eventData);
    }, onFailure);
};

StartlistController.startListAsJson = function(id, onSuccess, onFailure) {
    StartlistController.getStartlistData(id, onSuccess, onFailure);
};

function flattenArray(arr) {
    for (var i = 0;i < arr.length;i++) {
        arr[i] = flatten(arr[i]);
    }
}

StartlistController.getStartlistColumns = function(id = undefined) {
    if (id) {
        let node = getStartListNode(StartlistController.config.startLists, id);
        if (node != undefined && node.startlistOutputColumns) {
            return node.startlistOutputColumns;
        }
    }
    var outputColumns = StartlistController.config.startlistOutputColumns;
    return outputColumns;
}

StartlistController.startListAsCsv = function(id, outputColumns, onSuccess, onFailure, limitToRaceNumber) {
    StartlistController.getStartlistData(id, function(data, configOutputColumns) {
        var useOutputColumns = configOutputColumns != null ? configOutputColumns : outputColumns;
        var riderData = data.riderData;
        flattenArray(riderData);
        csvstringify(riderData, {
            header: true,
            maxDepth: 3,
            columns: useOutputColumns
        }, function (err, output) {
            if (err) {
                onFailure(err);
            } else {
                onSuccess(output);
            }
        });
    }, onFailure, limitToRaceNumber);
};


StartlistController.retrieveStartListData = function(startListNode, categoryListData, onSuccess, onFailure) {
    StartlistController.registrationListController.retrieveRegistrationListData(startListNode, categoryListData, function(eventData) {
        eventData.rankingData = StartlistController.registrationListController.retrieveRankingData(startListNode);
    }, onSuccess, onFailure);    
};

StartlistController.startListAsXlsReport = function(id, onSuccess, onFailure) {
    StartlistController.getStartlistData(id, function(eventData) {
        var xlsx = SLRC.convertEventToXls(eventData);
        onSuccess(xlsx);
    }, onFailure);
};

module.exports = StartlistController;