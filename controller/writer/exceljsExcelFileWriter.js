'use strict';

const ExcelJS = require('exceljs');

var ExcelJSExcelFileWriter = function() {

};

ExcelJSExcelFileWriter.createWorkbook = function() {
    const workbook = new ExcelJS.Workbook();
    return workbook;
};

ExcelJSExcelFileWriter.createWorksheet = function(workbook, sheetName) {
    const sheet = workbook.addWorksheet(sheetName);
    return sheet;
};

ExcelJSExcelFileWriter.setSheetHeader = function(worksheet, left, middle, right) {

};

ExcelJSExcelFileWriter.setSheetFooter = function(worksheet, left, middle, right) {

};

ExcelJSExcelFileWriter.setSheetToA4Portrait = function(worksheet) {
    worksheet.pageSetup = { paperSize: 9, orientation: 'portrait'};
};

ExcelJSExcelFileWriter.writeWorkbook = async function(workbook, outputPath) {
    console.log("Writing workbook " + outputPath);
    var p = Promise.resolve(workbook.xlsx.writeFile(outputPath));
    console.log("Returned writing workbook " + outputPath);
    //workbook.xlsx.writeFile(outputPath);
};


ExcelJSExcelFileWriter.writeWorkbookToStream = async function(workbook, stream) {
    await workbook.xlsx.write(stream);
};

module.exports = ExcelJSExcelFileWriter;