'use strict';
const csvstringify = require('csv-stringify');
const flatten = require('flat');
const findWithMatchingNode = require('../src/findWithMatchingNode');
//const findItemsWithMatchingNode = require('../src/findItemsWithMatchingNode');
const getRiderPrintId = require('../src/getRiderPrintId');
const DataFile = require('../api/datafile');
const dumpError = require('../src/dumpError');
const getDateAsISO = require('../src/getDateAsISO');
const isoDateOnly = require('../src/isoDateOnly');
const findItemsWithMatchingNode = require('../src/findItemsWithMatchingNode');
const getCategoryForRider = require('../src/getCategoryForRider');
const allFieldsMatch = require('../src/allFieldsMatch');
const excelTimeToHMS = require('../src/excelTimeToHMS');
const getStartListNode = require('../src/getStartListNode');

const DEBUG_CATEGORIES = ['Product'];

if (String.prototype.toProperCase == undefined) {
    String.prototype.toProperCase = function () {
        return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    };
}

function isCsvFile(dataFile) {
    return dataFile.endsWith(".csv");
}

const RegistrationListController = function() {
    RegistrationListController.config = {};
};

RegistrationListController.setConfig = function(configObject) {
    RegistrationListController.config = configObject;
};

RegistrationListController.setDataFileController = function(controller) {
    RegistrationListController.dataFileController = controller;
}

RegistrationListController.getRegoListColumns = function() {
    var outputColumns = RegistrationListController.config.regoListOutputColumns;
    return outputColumns;
}

RegistrationListController.getCategoriesFromStartlist = function(startListNode, onSuccess, onError) {
    var categoryListFile = startListNode.categories;
    var categoryDetailColumns = RegistrationListController.config.defaultCategoryDetailColumns;
    if (startListNode.categoryDetailColumns) {
        categoryDetailColumns = startListNode.categoryDetailColumns;
    }
    if (!categoryDetailColumns) {
        onError(Error("No category detail columns were defined either as default or for start list."));
        return;
    }

    RegistrationListController.dataFileController.dataFileAsJson(categoryListFile,
        function(data, hasHeaders) {
            if (hasHeaders) {
                parseColumnData(data, categoryDetailColumns, onSuccess, onError, categoryListFile);
            } else {
                onSuccess(data, categoryListFile);
            }
        }, onError
    );
};

RegistrationListController.getCategoryMappingFromStartlist = function(startListNode, onSuccess, onError) {
    var categoryMappingFile = startListNode.categoryMapping;
    var categoryMappingColumns = RegistrationListController.config.defaultCategoryMappingColumns;
    if (startListNode.categoryMappingColumns) {
        categoryMappingColumns = startListNode.categoryMappingColumns;
    }
    if (!categoryMappingColumns) {
        onError(Error("No category mappings were defined either as default or for start list."));
        return;
    }

    RegistrationListController.dataFileController.dataFileAsJson(categoryMappingFile,
        function(data, hasHeaders) {
            if (hasHeaders) {
                parseColumnData(data, categoryMappingColumns, function (outputData, sourceFile) {
                    console.log("Finished reading in CategoryMapping file " + sourceFile);
                    onSuccess(outputData, sourceFile);
                }, onError, categoryMappingFile);
            } else {
                onSuccess(data, categoryMappingFile);
            }
        }, onError
    );
};

function flattenArray(arr) {
    for (var i = 0;i < arr.length;i++) {
        arr[i] = flatten(arr[i]);
    }
}

RegistrationListController.registrationListAsJson = function(id, onSuccess, onFailure) {
    RegistrationListController.getRegistrationListData(id, function(data) {
            onSuccess(data.riderData);
        }, function (err, output) {
            if (err) {
                onFailure(err);
            } else {
                onSuccess(output);
            }
        });
}
 
RegistrationListController.registrationListAsCsv = function(id, outputColumns, onSuccess, onFailure) {
    RegistrationListController.getRegistrationListData(id, function(data) {
        var riderData = data.riderData;
        flattenArray(riderData);
        csvstringify(riderData, {
            header: true,
            maxDepth: 3,
            columns: outputColumns
        }, function (err, output) {
            if (err) {
                onFailure(err);
            } else {
                onSuccess(output);
            }
        });
    }, onFailure);
};

function addMappedCategories(eventCategories, categoryMappings, categoryDataFilePath, categoryMappingDataFilePath) {
    for (var catNo = 0;catNo < categoryMappings.length;catNo++) {
        var categoryMapping = categoryMappings[catNo];
        var matchingCategory = findWithMatchingNode(eventCategories, "CategoryCode", categoryMapping.CategoryCode);
        if (!matchingCategory) {
            var msg = categoryMapping.EntryDataCategory + " was specified in data file " + categoryMappingDataFilePath + " as mapping to code " + categoryMapping.CategoryCode + " which does not exist in event data.";
            console.warn(msg);
            //throw Error(msg);
        } else {
            addMappingToCategory(matchingCategory, categoryMapping);
        }
    }
}

function addMappingToCategory(eventCategory, categoryMapping) {
    if (!eventCategory.mappings) {
        eventCategory.mappings = [];
    }
    eventCategory.mappings.push(categoryMapping.EntryDataCategory);
}

function transformAllRiderData(eventData) {
    var dataArr = eventData.riderData;
    for (var i = 0;i < dataArr.length;i++) {
        var rider = dataArr[i];
        if (!rider) {
            continue;
        }
        if (rider.FirstName == null) {
            console.warn("A row was IGNORED in the data file because it contained no FirstName: " + JSON.stringify(rider));
            dataArr[i] = null;
        } else if (rider.Surname == null) {
            console.warn("A row was IGNORED in the data file because it contained no Surname: " + JSON.stringify(rider));
            dataArr[i] = null;
        } else {
            transformRiderData(eventData, rider);
        }
    }
}

function addAgeColumn(eventData, rider) {
    var age = getEntrantAge(eventData, rider);
    if (age) {
        rider.Age = age;
    }
}

function transformRiderData(eventData, rider) {
    if (rider.Surname == undefined || rider.FirstName == undefined) {
        let errMsg = `Rider container no surname data: ${JSON.stringify(rider)}`;

        if (rider.Surname == undefined && rider.FirstName == undefined) {
            errMsg = `Rider container no firstname or surname data: ${JSON.stringify(rider)}`;
        } else if (rider.FirstName == undefined) {
            errMsg = `Rider container no firstname data: ${JSON.stringify(rider)}`;
        }
        console.warn(errMsg);
        throw new Error(errMsg);
    }
    // Excel can make values things like booleans or dates.
    rider.Surname = rider.Surname.toString().toUpperCase();
    rider.FirstName = rider.FirstName.toString().toProperCase();

    try {
        if (!rider.Age) {
            addAgeColumn(eventData, rider);
        }
    } catch (err) {
        console.warn("Error getting rider age: " + err.message);
    }
}

function getCategoryData(startListNode, onSuccess, onFailure) {
    RegistrationListController.getCategoriesFromStartlist(startListNode, 
        function (categoryListData, categoryDataFilePath) {
            RegistrationListController.getCategoryMappingFromStartlist(startListNode,
                function (categoryMappingData, categoryMappingDataFilePath) {
                    addMappedCategories(categoryListData, categoryMappingData, categoryDataFilePath, categoryMappingDataFilePath);
                    onSuccess(categoryListData);
            }, onFailure);
    }, onFailure);
}

function applyCategoryToEntrant(entrant, category, eventDetails) {
    if (category.CategoryName) {
        entrant.EventCategoryName = category.CategoryName;
    }
    if (category.CategoryCode) {
        entrant.EventCategoryCode = category.CategoryCode;
    }
    validateEntrantAgeForCategory(entrant, category, eventDetails);
}


function validateLicenseExpiry(entrant, eventDate) {
    if (!entrant) {
        return;
    }
    var entrantLicenseExpiry = getDateAsISO(entrant.MembershipExpiryDate);
    entrantLicenseExpiry = new Date(entrantLicenseExpiry);
    if (entrantLicenseExpiry.getTime() < eventDate.getTime()) {
        addEntrantValidationError(entrant, "Entrant license expires on " + isoDateOnly(entrantLicenseExpiry));
    }
}

function validateLicenseExpiryDates(eventData) {
    var entrantDataArray = eventData.riderData;
    var eventDate = new Date(eventData.RaceDate);

    for (var i = 0;i < entrantDataArray.length;i++) {
        var entrant = entrantDataArray[i];

        validateLicenseExpiry(entrant, eventDate);
    }
}

function addEntrantValidationError(entrant, errStr) {
    if (!entrant.ValidationError) {
        entrant.ValidationError = Array();
    }
    entrant.ValidationError.push(errStr);
}

function getYearFromString(dateString) {
    var stringAsISO = getDateAsISO(dateString);
    var dateValue = new Date(stringAsISO);
    var yearValue = dateValue.getFullYear();
    return yearValue;
}

function getEntrantAge(eventDetails, entrant) {
    if (!entrant.DOB) {
        return null;
    }
    var raceDate = new Date();
    if (eventDetails.RaceDate) {
        raceDate = new Date(eventDetails.RaceDate);
    }
    var eventFullYear = raceDate.getFullYear();

    var entrantYOB = getYearFromString(entrant.DOB);
    var entrantAge = eventFullYear - entrantYOB;
    return entrantAge;
}

function validateEntrantAgeForCategory(entrant, category, eventDetails) {
    var entrantAge = getEntrantAge(eventDetails, entrant);

    if (category.MinAge && entrantAge < category.MinAge) {
        addEntrantValidationError(entrant, "Rider age is " + entrantAge + ", must be >" + category.MinAge + " to participate in " + category.CategoryName);
    }
    if (category.MaxAge && entrantAge > category.MaxAge) {
        addEntrantValidationError(entrant, "Rider age is " + entrantAge + ", must be <" + category.MaxAge + " to participate in " + category.CategoryName);
    }
}

function categoryMatches(checkName, category) {
    var lcCheckName = checkName.toLowerCase();
    if (category.CategoryName.toLowerCase() == lcCheckName) {
        return category;
    }
    if (category.mappings) {
        for (var j = 0;j < category.mappings.length;j++) {
            var currentMappingCategory = category.mappings[j];
            var currentMappingLcName = currentMappingCategory.toLowerCase();
            if (lcCheckName == currentMappingLcName) {
                return category;
            }
        }
    }
}

function findMatchingCategory(entrant, categoryListData, categoryField = 'Category') {
    var categoryFieldValue = entrant[categoryField];
    if (!categoryFieldValue) {
        console.info(`No category field (${categoryField}) found on entrant data for ${getRiderPrintId(entrant)}`);
        return null;
    }
    var entryDataCategory = categoryFieldValue.toLowerCase();
    for (var i = 0;i < categoryListData.length;i++) {
        var category = categoryListData[i];
        if (categoryMatches(entryDataCategory, category)) {
            return category;
        }
    }
    return null;
}

function applyCategoryToEntries(eventData) {
    var isOk = true;
    var entrantDataArray = eventData.riderData;
    var categoryListData = eventData.categoryList;
    for (var i = 0;i < entrantDataArray.length;i++) {
        var entrant = entrantDataArray[i];
        var matchingCategory = findMatchingCategory(entrant, categoryListData);
        if (matchingCategory) {
            applyCategoryToEntrant(entrant, matchingCategory, eventData);
        } else {
            addEntrantValidationError(entrant, "No valid category was found matching " + entrant.Category);
            isOk = false;
        }
    }
    return isOk;
}

function addRankingSetToRider(eventData, rider, rankingDataFile) {
    if (!rider) {
        return;
    }
    if (rider.EventCategoryCode && rankingDataFile.Category && rider.EventCategoryCode != rankingDataFile.Category) {
        return;
    }
    var rankingType = rankingDataFile.type;
    if (!rider.ranking) {
        rider.ranking = {};
    }

    var rankField = rankingDataFile.dataLinks['Rank'];
    var rankingPointsField = rankingDataFile.dataLinks['RankingPoints'];
    var matchingNodes;

    var plateNumberField = rankingDataFile.dataLinks['PlateNumber'];
    if (plateNumberField) {
        matchingNodes = matchRiderByPlateNumber(rider, rankingDataFile);
    }
    if (!matchingNodes || matchingNodes.length == 0) {
        matchingNodes = matchRiderByMemberNumber(rider, rankingDataFile);
    }
    if (matchingNodes && matchingNodes.length > 0) {
        //console.debug("Rider " + getRiderPrintId(rider) + " had " + matchingNodes.length + " " + rankingType + " ranking data fields (" + matchingNodes[0].Rank + ") by MemberNumber.");
    } else {
        matchingNodes = matchRiderByName(rider, rankingDataFile);
        if (matchingNodes.length > 0) {
            //console.debug("Rider " + getRiderPrintId(rider) + " had " + matchingNodes.length + " " + rankingType + " ranking data fields (" + matchingNodes[0].Rank + ") by name.");
        }
    }
    var categoryField = rankingDataFile.dataLinks['Category'];
    if (categoryField) {
        //var matchesBefore = matchingNodes;
        matchingNodes = getRankingsMatchingCategory(eventData, rider, matchingNodes, categoryField, rankField, rankingPointsField, rankingDataFile);
        //var matchesAfter = matchingNodes.length;
        //if (matchesBefore.length != matchesAfter) {
        //    console.debug("At least one row for " + getRiderPrintId(rider) + " was from a different category to what they have entered.");
        //}
    }

    if (matchingNodes.length > 0) {
        if (matchingNodes.length > 1) {
            console.debug("More than one " + rankingType + " ranking entry for " + getRiderPrintId(rider));
        }
        var firstNode = matchingNodes[0];

        var riderRank = firstNode[rankField];
        if (rankingType == 'nationalchampion') {
            rider.ranking[rankingType] = {'Rank': 2020};
        } else {
            rider.ranking[rankingType] = {'Rank': riderRank};
        }

        if (rankingType == 'timetrial') {
            rider.ranking[rankingType] = {Rank: riderRank};
            var timeField = rankingDataFile.dataLinks['Time'];
            if (timeField) {
                var timeFieldValue = firstNode[timeField];
                if (timeFieldValue < 1.0) {
                    timeFieldValue = "'" + excelTimeToHMS(timeFieldValue);
                }
                rider.ranking[rankingType].Time = timeFieldValue;
            }
        }

        if (rankingPointsField) {
            var riderPoints = firstNode[rankingPointsField];
            rider.ranking[rankingType].Points = riderPoints;
        }
    }

    if (rankingDataFile.category && rider.EventCategoryCode != rankingDataFile.category) {
        //Restrict to riders in this category.
        //console.log("Rider " + getRiderPrintId(rider) + " is not in matching category " + rankingDataFile.category);
        return;
    } else {
        var rankingValue = getRankingForRider(rider, rankingDataFile);
        rider.ranking[rankingType] = rankingValue;
    }
}

function getRankingForRider(rider, rankingDataFile) {
    var rankingType = rankingDataFile.type;
//    if (rankingType == "nationalcup") {
        // match on category
        var rankingValue = rider.ranking[rankingType];
        return rankingValue;
//    }
//    return null;
}

function matchRiderByPlateNumber(rider, rankingDataFile) {
    var plateNumberField = rankingDataFile.dataLinks['PlateNumber'];
    if (plateNumberField) {
        var matchingNodes = findItemsWithMatchingNode(rankingDataFile.data, plateNumberField, rider.PlateNumber);
        return matchingNodes;
    }
    return null;
}

function matchRiderByMemberNumber(rider, rankingDataFile) {
    var memberNumberField = rankingDataFile.dataLinks['MemberNumber'];
    if (memberNumberField) {
        var matchingNodes = findItemsWithMatchingNode(rankingDataFile.data, memberNumberField, rider.MemberNumber);
        return matchingNodes;
    }
    return null;
}

function matchRiderFirstnameSurname(rider, firstname, surname) {
    if (!firstname) {
        return false;
    }
    if (!surname) {
        return false;
    }
    var riderFirstname = rider.FirstName.toLowerCase().trim();
    var riderSurname = rider.Surname.toLowerCase().trim();
    if (riderFirstname != firstname.toLowerCase().trim()) {
        return false;
    }
    if (riderSurname != surname.toLowerCase().trim()) {
        return false;
    }
    return true;
}

function matchRiderByName(rider, rankingDataFile) {
    var firstnameField = rankingDataFile.dataLinks['FirstName'];
    var surnameField = rankingDataFile.dataLinks['Surname'];
    var fullNameField = rankingDataFile.dataLinks['FullName'];
    var results = Array();
    var data = rankingDataFile.data;
    for (var i = 0;i < data.length;i++) {
        var item = data[i];
        var firstname = item[firstnameField];
        var surname = item[surnameField];
        if (matchRiderFirstnameSurname(rider, firstname, surname)) {
            results.push(item);
        }
        else if (item && item[firstnameField] && item[firstnameField].trim().toLowerCase() == rider.FirstName.toLowerCase()) {
            if (item && item[surnameField] && item[surnameField].trim().toLowerCase() == rider.Surname.toLowerCase()) {
                results.push(item);
            }
        }
        if (item && fullNameField && item[fullNameField]) {
            var fullnameRev = rider.Surname.trim() + " " + rider.FirstName.trim();
            var fullnameFwd = rider.FirstName.trim() + " " + rider.Surname.trim();
            var fullname = item[fullNameField].trim().toLowerCase();
            if (fullname == fullnameRev.toLowerCase() || fullname == fullnameFwd.toLowerCase()) {
                results.push(item);
            }
        }
    }
    return results;
}

function addFieldsToExistingData(existingData, newData, fieldsToCopy) {
    for (var i = 0; i < fieldsToCopy.length;i++) {
        var field = fieldsToCopy[i];
        existingData[field] = newData[field];
    }
}

function joinProcessExistingRow(existingDataRow, fieldSet, joinData) {
    var caseSensitive = fieldSet.caseSensitive != null ? false : fieldSet.caseSensitive;
    for (var i = 0;i < fieldSet.on.length;i++) {
        var joinOnSet = fieldSet.on[i];
        var rowAlreadyJoined = false;

        for (var joinRowNumber = 0;joinRowNumber < joinData.length;joinRowNumber++) {
            var joinRowToCheck = joinData[joinRowNumber];
            if (allFieldsMatch(existingDataRow, joinRowToCheck, joinOnSet, caseSensitive))
            {
                addFieldsToExistingData(existingDataRow, joinRowToCheck, fieldSet.fields);
                rowAlreadyJoined = true;
                break;
            }
        }
        if (rowAlreadyJoined) {
            break;
        }
    }
}

function joinOnFields(eventData, joinNode, joinData) {
    for (var i = 0;i < eventData.riderData.length;i++) {
        var row = eventData.riderData[i];
        joinProcessExistingRow(row, joinNode, joinData);
    }
}

function createJoinFieldInfoString(joinNode) {
    var joinStr = JSON.stringify(joinNode.on);
    return joinStr;
}

function processJoin(eventData, joinNode) {
    var joinFile = joinNode.file;
    var sheet = joinNode.sheet;
    var joinData = RegistrationListController.dataFileController.dataFileAsJsonSync(joinFile, function (joinData) {
        var logMsg = "Joining file " + joinFile;
        if (sheet) {
            logMsg += " (Sheet: " + sheet + ")";
        }
        logMsg += ' on: ' + createJoinFieldInfoString(joinNode);
        console.log(logMsg);
//        console.log("Got data to join: " + JSON.stringify(joinData));
        joinOnFields(eventData, joinNode, joinData);
    }, function (error) {
        console.log("Error parsing join file " + joinFile);
        if (joinNode != undefined && joinNode.optional == false) {
            throw error;
        }
    }, sheet, null);
}

function joinData(eventData, startListNode) {
     var joins = startListNode.join;
     if (!joins) {
         return;
     }
     for (var i = 0;i < joins.length;i++) {
         var joinNode = joins[i];
         processJoin(eventData, joinNode);
     }
     console.log("Finished joining " + joins.length + " data files.");
}

function getRankingsMatchingCategory(eventData, rider, matchingNodes, categoryField, rankField, rankingPointsField, rankingDataFile, showWarnings = false) {
    var output = Array();
    var entryCategory = getCategoryForRider(eventData, rider, DEBUG_CATEGORIES);
    if (!entryCategory && rider.EventCategoryCode == undefined && rider.Category != undefined) {
        console.warn(`Category '${rider.Category}' was not recognised and did not find a mapped code for ${getRiderPrintId(rider)}`);
    } else if (!entryCategory && rider.Category != undefined) {
        console.warn("Category Mapping: '" + rider.Category + "' not recognised or mapped for " + getRiderPrintId(rider) + " in mapped category " + rider.EventCategoryCode);
    } else if (rider.Category != undefined) {
        for (var i = 0;i < matchingNodes.length;i++) {
            var rankingNode = matchingNodes[i];
            var matches = categoryMatches(rankingNode[categoryField], entryCategory);
            if (matches) {
                output.push(rankingNode);
            } else {
                var rankInfo;
                var riderRank = rankingNode[rankField];

                if (rankingPointsField) {
                    var riderPoints = rankingNode[rankingPointsField];
                    rankInfo = "#" + riderRank + " (" + riderPoints + ")";
                } else {
                    rankInfo = "#" + riderRank;
                }
                
                var regoMsg = "Ranking data (" + rankInfo + ") in category " + rankingNode[categoryField] + " was discarded due to category change.";
                addEntrantValidationError(rider, regoMsg);
                if (showWarnings) {
                    console.warn("Ranking: " + getRiderPrintId(rider) + " in " + rankingNode[categoryField] + " discarded for entry category " + entryCategory.CategoryName + " while processing file " + rankingDataFile.file);
                }
            }
        }
    }
    return output;
}

function applyRankingToEntries(eventData) {
    var rankingData = eventData.rankingData;
    var riderData = eventData.riderData;
    for (var i = 0;i < riderData.length;i++) {
        var rider = riderData[i];
        for (var r = 0;r < rankingData.length;r++) {
            var rankingDataFile = rankingData[r];
            addRankingSetToRider(eventData, rider, rankingDataFile);
        }
    }
}

RegistrationListController.retrieveRankingData = function(startList) {
    var rankingDataArr = startList.rankingData;

    var result = Array();
    for (var i = 0;i < rankingDataArr.length;i++) {
        var rankingDataElement = rankingDataArr[i];
        var importFile = rankingDataElement.file;
        var worksheet = rankingDataElement.worksheet;
        RegistrationListController.dataFileController.dataFileAsJsonSync(importFile, function (data) {
            rankingDataElement.data = data;
            result.push(rankingDataElement);
        }, function (err) {
            throw Error(err);
        }, worksheet);
    }
    return result;
};

// RegistrationListController.getStartListNode = function(id) {
//     var startListNode = findById(RegistrationListController.config.startLists, id);
//     startListNode = startListNode.get();
//     return startListNode;
// }

RegistrationListController.getRegistrationListData = function(id, onSuccess, onFailure) { 
    var startListNode = getStartListNode(RegistrationListController.config.startLists, id);
    getCategoryData(startListNode, function (categoryListData) {
        RegistrationListController.retrieveRegistrationListData(startListNode, categoryListData, function(eventData) {
            transformAllRiderData(eventData, startListNode);
        }, function (eventData) {
            // apply category data to start list.
            eventData.rankingData = RegistrationListController.retrieveRankingData(startListNode);
            applyRankingToEntries(eventData);
            validateLicenseExpiryDates(eventData);
        
            transformAllRiderData(eventData);
        
            eventData.riderData = eventData.riderData.filter(function (el) {
                return el != null;
            });
              
            onSuccess(eventData, startListNode);
        },
        onFailure);
    }, onFailure);
};

function parseColumnData(data, columns, onSuccess, onError, sourceFile) {
    var headers = data[0];
    try {
        var dataFileDef = DataFile(columns, headers);
        var outputData = Array();
        for (var csvRow = 1;csvRow < data.length;csvRow++) {
            var obj = dataFileDef.objectFromRow(data[csvRow]);
            outputData.push(obj);
        }
    } catch (err) {
        onError(err);
    }
    onSuccess(outputData, sourceFile);
}

function retrieveRaces(startListNode, eventData, onComplete, onFailure) {
    var racesFile = startListNode.races.file;

    var racesColumns = startListNode.races.columns;
    if (!racesColumns) {
        racesColumns = RegistrationListController.config.defaultRacesFileColumns;
    }

    RegistrationListController.dataFileController.dataFileAsJson(racesFile, function(data, hasHeaders) {
        if (hasHeaders) {
            var headers = data[0];
            var dataFileDef = DataFile(racesColumns, headers);
            eventData.racesData = Array();
            for (var csvRow = 1;csvRow < data.length;csvRow++) {
                var raceObject = dataFileDef.objectFromRow(data[csvRow]);
                eventData.racesData.push(raceObject);
            }
        } else {
            eventData.racesData = data;
        }
        if (onComplete) {
            onComplete();
        }
    }, onFailure);
}

function retrieveRacesPromise(startListNode, eventData) {
    var dataPromise = new Promise((resolve, reject) => {

        var racesFile = startListNode.races.file;

        var racesColumns = startListNode.races.columns;
        if (!racesColumns) {
            racesColumns = RegistrationListController.config.defaultRacesFileColumns;
        }

        RegistrationListController.dataFileController.dataFileAsJson(racesFile, function(data, hasHeaders) {
            if (hasHeaders) {
                var headers = data[0];
                var dataFileDef = DataFile(racesColumns, headers);
                eventData.racesData = Array();
                for (var csvRow = 1;csvRow < data.length;csvRow++) {
                    var raceObject = dataFileDef.objectFromRow(data[csvRow]);
                    eventData.racesData.push(raceObject);
                }
            } else {
                eventData.racesData = data;
            }
            resolve();
        }, reject);
        });
    return dataPromise;
}

function isFiltered(filter, rider) {
    var keys = Object.keys(filter);
    for (var k = 0;k < keys.length;k++) {
        var key = keys[k];
        let filterData = filter[key];
        if (Array.isArray(filterData)) {
            let filtered = false;
            filterData.forEach((v) => {
                if (rider[key] == v) {
                    filtered = true;
                } else if (v == "" && !rider[key]) {
                    filtered = true;
                }
            });
            if (filtered) {
                return true;
            }
        } else if (rider[key] == filterData) {
            return true;
        }
    }
    return false;
}

function shouldExcludeRow(excludes, rider) {
    for (var n = 0;n < excludes.length;n++) {
        var exc = excludes[n];
        if (isFiltered(exc, rider)) {
            return true;
        }
    }
    return false;
}

function usesDataFilesArray(startListNode) {
    if (startListNode.dataFiles && Array.isArray(startListNode.dataFiles) && startListNode.dataFiles.length > 0) {
        return true;
    }
    return false;
}

function appendDayEntries(eventData, startListNode) {
    if (usesDataFilesArray(startListNode)) {
        throw Error("Should not be using old apply day entries code when using dataFiles array.");
    }
    var dayEntriesFile = startListNode.dayEntries;
    if (!dayEntriesFile) {
        return;
    }
    var excludes = startListNode.excludeData;
    RegistrationListController.dataFileController.xlsxAsJson(dayEntriesFile, function(data) {
        for (var n = 0;n < data.length;n++) {
            var dataRow = data[n];
            if (excludes && shouldExcludeRow(excludes, dataRow)) {
                console.log("Excluded rider " + getRiderPrintId(dataRow) + " from day file based on filter.");
            } else {
                eventData.riderData.push(dataRow);
            }
        }
    }, function(err) {
        dumpError(err);
    }, null, null);
}

function addRowToEventData(eventData, excludeData, riderObject) {
    if (excludeData && shouldExcludeRow(excludeData, riderObject)) {
        console.log("Excluded rider " + getRiderPrintId(riderObject) + " from primary file based on filter.");
    } else {
        eventData.riderData.push(riderObject);
    }
}

function hasData(obj, fieldName) {
    return obj[fieldName] != undefined;
}

function dataHasColumn(dataArray, fieldName) {
    for (var rowNumber = 1;rowNumber < dataArray.length;rowNumber++) {
        var data = dataArray[rowNumber];
        if (hasData(data, fieldName)) {
            return true;
        }
    }
    return false;
}

function hasColumnMapping(dataFileDetails, fieldName) {
    if (!dataFileDetails.dataLinks) {
        return false;
    }
    if (dataFileDetails[fieldName]) {
        return true;
    }
    return false;
}

function excludeLinkField(elementName) {
    if (elementName.startsWith("*")) {
        return true;
    }
    return false;
}

function applyDataMappings(dataFileDetails, riderObject) {
    if (dataFileDetails.dataLinks != undefined) {
        for (const [key, value] of Object.entries(dataFileDetails.dataLinks)) {
            if (riderObject[key] != undefined) {
                throw Error('Could not map column ' + value + ' to ' + key + ' because it already contains data value "' + riderObject[key] + '"');
            } else if (Array.isArray(value)) {
                var hasSet = false;
                value.forEach(element => {
                    if (!excludeLinkField(element)) {
                        if (riderObject[element] != undefined && riderObject[element] != "" && !hasSet) {
                            riderObject[key] = riderObject[element];
                            delete riderObject[element];
                            hasSet = true;
                        } else {
                            delete riderObject[element];
                        }
                    }
                });
                if (!hasSet) {
                    //console.warn(`Rider object didn't have a field to match for ${key}: ${JSON.stringify(riderObject)}`);
                    riderObject[key] = undefined;
                }
            } else {
                riderObject[key] = riderObject[value];
                delete riderObject[value];
            }
        }
    } else {
        console.log("No data links to apply.");
    }
}

function processRegistrationDataFile(eventData, startListNode, dataFileDetails, beforeJoinData, onFailure) {
    if (!dataFileDetails.file) {
        throw Error('Data file detail object defined in config does not reference a file, so can not read any data: ' + JSON.stringify(dataFileDetails));
    }
    var excludeData = startListNode.excludeData;
    var dataFile = dataFileDetails.file;
    console.log(`Importing data file ${dataFile}`);

    var isCsv = isCsvFile(dataFile);
    var riderDetailColumns = getRiderDetailColumns(startListNode);

    RegistrationListController.dataFileController.dataFileAsJsonSync(dataFile, function(data) {
        var headers = data[0];
        var dataFileDef;
        if (isCsv) {
            dataFileDef = DataFile(riderDetailColumns, headers);
        } else {
            if (!dataHasColumn(data, 'Category') && hasColumnMapping(dataFileDetails, 'Category')) {
                throw Error('No category field in data file.');
            }
        }
        if (!eventData.riderData) {
            eventData.riderData = Array();
        }

        for (var rowNumber = 1;rowNumber < data.length;rowNumber++) {
            var riderObject = data[rowNumber];
            if (isCsv) {
                riderObject = dataFileDef.objectFromRow(riderObject);
            }
            applyDataMappings(dataFileDetails, riderObject);

            if ((dataFileDetails.matchRequired && isFiltered(dataFileDetails.matchRequired, riderObject)) || !dataFileDetails.matchRequired) {
                if (dataFileDetails.filterOut && isFiltered(dataFileDetails.filterOut, riderObject)) {
                    console.debug("Data row for " + getRiderPrintId(riderObject) + " was filtered out explicitly." + riderObject.XCCCategory);
                } else {
                    addRowToEventData(eventData, excludeData, riderObject);
                    console.debug("Added " + getRiderPrintId(riderObject) + " (" + riderObject.Category + ")");
                }
            } else {
                console.debug("Data row for " + getRiderPrintId(riderObject) + " was filtered (" + riderObject.XCCCategory + ")");
            }
        }
    }, onFailure, startListNode.worksheet, startListNode.header);
}

function getRiderDetailColumns(startListNode) {
    var riderDetailColumns = startListNode.riderDetailColumns;
    if (!riderDetailColumns) {
        riderDetailColumns = RegistrationListController.config.defaultRiderDetailColumns;
    }
    return riderDetailColumns;
}

function applyFilter(eventData, startListNode) {

}

RegistrationListController.retrieveRegistrationListData = function(startListNode, categoryListData, beforeJoinData, onSuccess, onFailure) {
    var dataFile = startListNode.dataFile;
    var dataFiles = startListNode.dataFiles;
    var eventData = {
        "RaceDate": "2022-12-31"
    };

    eventData.categoryList = categoryListData;

    var retrieveRacesPr = retrieveRacesPromise(startListNode, eventData);
    
    try {
    //retrieveRacesPr.then(() => {
        if (dataFiles) {
            if (!Array.isArray(dataFiles)) {
                throw Error("config.dataFiles is set but must be an array.");
            } else if (dataFiles.length < 1) {
                throw Error("config.dataFiles is an empty array.");
            }
            for (var df = 0;df < dataFiles.length;++df) {
                var currentDataFile = dataFiles[df];
                processRegistrationDataFile(eventData, startListNode, currentDataFile, beforeJoinData, onFailure);
            }
            if (dataFile) {
                console.log("Skipping defined dataFile because dataFiles array was specified.");
            }
        } else {
            processRegistrationDataFile(eventData, startListNode, dataFile, beforeJoinData, onFailure);
            if (!usesDataFilesArray(startListNode)) {
                console.log("Applying day entry file.");
                appendDayEntries(eventData, startListNode);
            }
        }
        applyCategoryToEntries(eventData);
    
        if (beforeJoinData) {
            beforeJoinData(eventData, startListNode);
        }

        console.log("Joining post-load additional data files.");
        joinData(eventData, startListNode);

        applyFilter(eventData, startListNode);

        onSuccess(eventData);
//    }).catch((err) => {
} catch (err) {
        console.warn(err.message);
        console.trace(`${JSON.stringify(err.stack)}`);
        onFailure(err);
        //onSuccess(err);
//    });
}
};

module.exports = RegistrationListController;