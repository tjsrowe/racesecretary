'use strict';

const os = require('os');
const getCategoryForRider = require('../src/getCategoryForRider');
const excel = require('./writer/exceljsExcelFileWriter');

var config;
var StartlistReportController = function() {};
var SLC;

StartlistReportController.setConfig = function(conf) {
    config = conf;
}

StartlistReportController.setStartlistController = function(controller) {
    SLC = controller;
}

function getCategoriesInRace(eventData, race) {
    var result = Array();
    for (var i = 0;i < eventData.categoryList.length;i++) {
        var cat = eventData.categoryList[i];
        if (cat.RaceNumber == race.RaceNumber) {
            result.push(cat);
        }
    }
    return result;
}

function getRidersInRace(eventData, race) {
    var ridersInRace = Array();
    //var categories = getCategoriesInRace(eventData, race);
    for (var i = 0;i < eventData.riderData.length;i++) {
        var rider = eventData.riderData[i];
        var isInRace = SLC.isRiderInRace(eventData, rider, race);
        if (isInRace) {
            ridersInRace.push(rider);
        }
    }
    return ridersInRace;
}

function addRaceToWorkbook(book, eventData, race) {
    var raceRiders = getRidersInRace(eventData, race);

    var worksheetName = "Race" + race.RaceNumber;
 
    /* make worksheet */
    var ws_data = [
    [ ],
    [  ]
    ];

    var sheet = excel.createWorksheet(book, worksheetName);
    //var sheet = XLSX.utils.aoa_to_sheet(ws_data);

    /* Add the worksheet to the workbook */
    //XLSX.utils.book_append_sheet(book, sheet, worksheetName);
}

function addRacesToWorkbook(book, eventData, restrictToRaces) {
    for (var i = 0;i < eventData.racesData.length;i++) {
        var race = eventData.racesData[i];
        addRaceToWorkbook(book, eventData, race);
    }
}

StartlistReportController.convertEventToXls = function(eventData, restrictToRaces, outputStream) {
    //const wb = XLSX.utils.book_new();
    const workbook = excel.createWorkbook();
    var outputPath = os.tmpdir() + "startlist_tmp.xlsx";

    addRacesToWorkbook(workbook, eventData);
    if (outputStream) {
        excel.writeWorkbookToStream(workbook, outputStream);
    } else {
        excel.writeWorkbook(workbook, outputPath);
        console.log("Returning path of written excel file " + outputPath);
    }
    
    //XLSX.writeFile(wb, tmpFile);
    return outputPath;
};

module.exports = StartlistReportController;