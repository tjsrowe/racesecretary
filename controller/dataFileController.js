'use strict';
const { doesNotMatch } = require('assert');
const csvReader = require('csv-parse');
var config = {};
const fs = require('fs');
//const xlsxutils = require('XLSX.utils');
const XLSX = require('XLSX');
const { parserConfiguration } = require('yargs');
const dumpError = require('../src/dumpError');
const { finished } = require('stream/promises');

var DataFileController = function() {

};

function hasFiletypeInFile(filePath, extension) {
    if (!filePath || !extension) {
        throw Error("You must specify both a file path and extension to check");
    }
    var extensionPosition = filePath.indexOf("." + extension);
    if (extensionPosition > 0) {
        return true;
    }
    return false;
}

function isXlsFile(filePath) {
    if (hasFiletypeInFile(filePath, "xlsx") || hasFiletypeInFile(filePath, "xls")) {
        return true;
    }
    return false;
}

function excelFilenameHasSheet(filePath) {
    var sheetPosition = filePath.indexOf("!");
    if (sheetPosition >= 0) {
        return true;
    }
    return false;
}

function getExcelSheetFromPath(filePath) {
    var sheetPosition = filePath.indexOf("!");
    if (sheetPosition > 0) {
        var sheetName = filePath.substring(sheetPosition+1);
        return sheetName;
    }
    return null;
}

function getExcelFilenameForPath(filePath) {
    if (excelFilenameHasSheet(filePath)) {
        var sheetPosition = filePath.indexOf("!");
        var xlsFileName = filePath.substring(0, sheetPosition);
        return xlsFileName;
    }
    return filePath;
}

function processXlsFile(filePath, onSuccess, onFailure, worksheet, header) {
    if (worksheet) {
        DataFileController.xlsxAsJson(filePath, onSuccess, onFailure, worksheet, header);
    } else {
        if (excelFilenameHasSheet(filePath)) {
            var xlsxPath = getExcelFilenameForPath(filePath);
            var sheet = getExcelSheetFromPath(filePath);
            DataFileController.xlsxAsJson(xlsxPath, onSuccess, onFailure, sheet, header);
        } else {
            DataFileController.xlsxAsJson(filePath, onSuccess, onFailure, worksheet, header);
        }
    }
}

DataFileController.dataFileAsJson = function(filePath, onSuccess, onFailure, worksheet, header) {
    if (isXlsFile(filePath)) {
        processXlsFile(filePath, onSuccess, onFailure, worksheet, header);
    } else {
        DataFileController.csvAsJson(filePath, onSuccess, onFailure);
    }
};

DataFileController.dataFileAsJsonSync = function(filePath, onSuccess, onFailure, worksheet, header) {
    if (isXlsFile(filePath)) {
        processXlsFile(filePath, onSuccess, onFailure, worksheet, header);
    } else {
        DataFileController.csvAsJsonSync(filePath, onSuccess, onFailure);
    }
};

DataFileController.csvAsJsonSync = function(filePath, onSuccess, onFailure) {
    if (!fs.existsSync(filePath)) {
        onFailure(Error("File " + filePath + " does not exist."));
    }
    try {
        var stream = fs.createReadStream(filePath);
        var jsonData = DataFileController.syncCsvAsJson(stream);
        onSuccess(jsonData);
    }
    catch (err) {
        onFailure(err);
    }
};

DataFileController.syncCsvAsJson = function(stream) {
    var records = [];

    var parser = stream.pipe(csvReader({}));

    parser.on('readable', function() {
        let record;
        while ((record = parser.read()) != null) {
          records.push(record);
        }
    });
    //await finished(parser);
    return records;
};

DataFileController.csvAsJson = function(filePath, onSuccess, onFailure) {
  if (!fs.existsSync(filePath)) {
    onFailure(Error("File " + filePath + " does not exist."));
  }
    var fileContents = fs.readFileSync(filePath);
    try {
    csvReader(fileContents, {

      }, function(err, output) {
        if (err) {
          onFailure(err);
        } else {
            try {
                onSuccess(output, true);
            } catch (err) {
                console.debug(err);
            }
        }
      });
    } catch(err) {
        dumpError(err);
      }
};

function removeSpaces(str) {
    return str.replace(' ', '');
}

DataFileController.xlsxAsJson = function(filePath, onSuccess, onFailure, worksheet, header) {
    if (!fs.existsSync(filePath)) {
        onFailure(Error("File " + filePath + " does not exist."));
    }
    //var buf = fs.readFileSync("sheetjs.xlsx");
    //var workbook = XLSX.readFile(buf, {type: 'buffer'});
    var workbook = XLSX.readFile(filePath);
    var sheet = null;
    if (worksheet == null) {
        var sheetKeys = Object.keys(workbook.Sheets);
        sheet = workbook.Sheets[sheetKeys[0]];
    } else if (workbook.Sheets[worksheet]) {
        sheet = workbook.Sheets[worksheet];
    } else {
        for (var i = 0;i < workbook.Sheets.length;i++) {
            var checkWorksheet = workbook.Sheets[i];
            if (checkWorksheet.SheetNames[i] == worksheet) {
                sheet = workbook.Sheets[i];
                break;
            }
        }
    }
    if (sheet) {
        var json;
        var csv;
        if (header) {
            var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
            //json = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], {"header": header});
            // workbook.Sheets[workbook.SheetNames[0]]
            //json = XLSX.utils.sheet_to_json(sheet, {header: 1, blankRows: false})
            var rows = Array();
            for (var j = 1;j < 4096;j++) {
                var row = Array();
                var obj = {};
                for (var r = 0;r < letters.length;r++) {
                    var col = letters[r];
                    var cell = col + j;
                    if (!sheet[cell]) {
                        break;
                    }
                    var value = sheet[cell].v;

                    var headerKey = removeSpaces(header[r]);
                    obj[headerKey] = value;

                    row.push(value);
                }
                if (row.length > 0) {
                    //rows.push(row);
                    rows.push(obj);
                }
            }
            sheet.ref = 'A1:G1200';
            //json = XLSX.utils.sheet_to_json(sheet, {header: ["Rank", "UCIID", "Name", "Nationality", "TeamCode", "Age", "Points"], blankrows: true});
            //var range = sheet.Range("A1:G1200");
            //csv = XLSX.utils.sheet_to_csv(sheet, {range: "A1:G1200"});
            json = rows;
        } else {
            json = XLSX.utils.sheet_to_json(sheet);   
        }
        
        onSuccess(json);
    } else {
        var msg = `No worksheet ${worksheet} found in file ${filePath}`;
        console.warn(msg);
        onFailure(msg);
    }
}

DataFileController.categoryDetailsAsJson = function(file, onSuccess, onFailure) {

};

DataFileController.setConfig = function(configObject) {
    config = configObject;
};

DataFileController.dataLineToRider = function(columnNumbers, arr) {

};

module.exports = DataFileController;